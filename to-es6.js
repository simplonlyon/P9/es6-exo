// var quotes = array('ga', 'zo', 'bu', 'meu');
const quotes = ['ga', 'zo', 'bu', 'meu'];


// var found = [];
// for(var i = 0; i < quotes.length; i++) {
//     if(quotes[i].length > 2) {
//         found.push(quotes[i]);
//     }
// }
let found = quotes.filter(item => item.length > 2);

const remotePush = async ({method, url, body}) => {
    const response = await fetch(url, {
        method,
        body: JSON.parse(body)
    });

    return response.json();
    
}

if(found.length > 1) {
    remotePush({
        url: 'http://url-du-back',
        method: 'POST',
        body: quotes
    }).then(data => found = [...found, data] );
    
}
